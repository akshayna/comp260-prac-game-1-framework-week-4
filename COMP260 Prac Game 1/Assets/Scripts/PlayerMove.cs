﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	// in metres per second
	public float acceleration = 7.0f;
	// in metres/second/second
	private float speed = 0.0f;
	// in metres/second
	public float brake = 5.0f;
	// in metres/second/second
	public float turnSpeed = 60.0f;
	// in degrees/second
	public float destroyRadius = 1.0f;

	private BeeSpawner beeSpawner;

	void Start() {
		// find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}


	void Update (){

		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = Input.GetAxis ("Horizontal");

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis ("Vertical");

		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
			transform.Rotate (0, 0, -turn * turnSpeed * speed * Time.deltaTime);
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
			transform.Rotate (0, 0, turn * turnSpeed * speed * Time.deltaTime);
		} else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
				if (speed < 0) {
					speed = 0;
				}



			} else {
				speed = speed + brake * Time.deltaTime;
				if (speed > 0) {
					speed = 0;
				}

			}
		}

		// clamp the speed
		speed = Mathf.Clamp (speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate (velocity * Time.deltaTime, Space.Self);
	}

}